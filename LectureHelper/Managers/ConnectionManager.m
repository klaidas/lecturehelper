//
//  ConnectionManager.m
//  lecturehelper
//
//  Created by kls on 11/14/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "ConnectionManager.h"
#import "CoreManager.h"
#import "Post.h"
#import "User.h"

#define PostClass @"Post"
#define MessageType @"lecture_type"
#define MessageAttachment @"message_attach"
#define MessageContent @"message_content"
#define MessageDate @"message_date"
#define MessageId @"message_id"
#define MessageModuleCode @"module_code"
#define MessageUserName @"user_name"

#define ServerURL @"http://studentsktudo.96.lt/"

@interface ConnectionManager() <NSURLSessionDelegate>

@property (strong, nonatomic) NSOperationQueue *operationQueue;

@end

@implementation ConnectionManager

- (NSOperationQueue *)operationQueue
{
    if (!_operationQueue)
    {
        _operationQueue = [NSOperationQueue new];
    }
    return _operationQueue;
}

- (void)fetchDataForModule:(NSString *)module lectureType:(NSString *)type completionHandler:(void(^)(NSData *, NSURLResponse *, NSError *))completionHandler
{
    NSString *code = module;
    
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:code forKey:@"module_code"];
    [dic setValue:type forKey:@"lecture_type"];
    
    
    NSURL *url = [NSURL URLWithString:ServerURL];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    
   // [NSURLConnection sendAsynchronousRequest:<#(nonnull NSURLRequest *)#> queue:<#(nonnull NSOperationQueue *)#> completionHandler:<#^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError)handler#>]
    
    
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:self.operationQueue];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:completionHandler];

    [task resume];
}

- (void)createMessage:(NSString *)message module:(NSString *)module type:(NSString *)type completionHandler:(void(^)(NSData *, NSURLResponse *, NSError *))completionHandler
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:module forKey:@"module_code"];
    [dic setValue:type forKey:@"lecture_type"];
    [dic setValue:message forKey:@"message_content"];
    [dic setValue:[User mainUser].name forKey:@"user_name"];
    
    
    NSURL *url = [NSURL URLWithString:ServerURL];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    
    // [NSURLConnection sendAsynchronousRequest:<#(nonnull NSURLRequest *)#> queue:<#(nonnull NSOperationQueue *)#> completionHandler:<#^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError)handler#>]
    
    
    //NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:self.operationQueue];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:completionHandler];
    
    [task resume];

}

- (void)insertPosts:(NSArray *)posts toModule:(Module *)module
{
    if (!posts || !posts.count)
    {
        return;
    }
    
    for (NSDictionary *dictionary in posts)
    {
        if ([self postWIthIDExists:[dictionary valueForKey:MessageId]])
        {
            continue;
        }
        
        Post *newPost = [NSEntityDescription insertNewObjectForEntityForName:PostClass inManagedObjectContext:[[CoreManager sharedInstance] context]];
        
        newPost.type = dictionary[MessageType];
        newPost.content = dictionary[MessageContent];
        newPost.attachment = dictionary[MessageAttachment];
        newPost.date = [self dateFromDatabaseDate:dictionary[MessageDate]];
        newPost.id = dictionary[MessageId];
        newPost.module_code = dictionary[MessageModuleCode];
        newPost.user_name = dictionary[MessageUserName];
        newPost.module = module;
    }
    
    [[[CoreManager sharedInstance] context] save:nil];
    
}

- (NSDate *)dateFromDatabaseDate:(NSString *)dbDate
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [formatter dateFromString:dbDate];
}

- (BOOL)postWIthIDExists:(NSString *)postID
{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:PostClass];
    request.predicate = [NSPredicate predicateWithFormat:@"id == %@", postID];
    
    NSArray *results = [[[CoreManager sharedInstance] context] executeFetchRequest:request error:nil];
    
    if (results && results.count)
    {
        return YES;
    }
    
    return NO;
}

@end
     
