//
//  CoreManager.h
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/10/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CoreManager : NSObject

@property (strong, nonatomic) NSManagedObjectContext *context;

+ (instancetype)sharedInstance;

- (void)logout;
- (BOOL)loggedIn;
- (void)loginWithCredentials:(NSString *)credentials modules:(NSString *)modules completionHandler:(void (^)(BOOL))completionHandler;

@end
