//
//  CoreManager.m
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/10/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "CoreManager.h"
#import "User.h"

#define StringSeparator @","

@interface CoreManager()

@property (strong, nonatomic) NSManagedObjectModel *model;


@end

@implementation CoreManager

#pragma mark - Init

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static CoreManager *sharedInstance;
    dispatch_once(&once, ^
                  {
                      sharedInstance = [[self alloc] init];
                  });
    return sharedInstance;
}

#pragma mark - CoreData

- (NSManagedObjectModel *)model
{
    if (!_model)
    {
        NSURL *modelURL  = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
        _model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    return _model;
}

- (NSManagedObjectContext *)context
{
    if (!_context)
    {
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
        NSURL *storeURL =  [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"db.sqlite"];
        NSError *error = nil;
        [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
        
        if (error)
        {
            NSLog(@"error while adding persistant store to coordinator: %@", error);
        }
        
        _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_context setPersistentStoreCoordinator:coordinator];
        
    }
    return _context;
}

#pragma mark - Login

- (BOOL)loggedIn
{
    if ([User mainUser])
    {
        return YES;
    }
    
    return NO;
}

- (void)logout
{
    NSArray *allObjects = [NSArray new];
    
    NSFetchRequest * fetch = [NSFetchRequest new];
    [fetch setEntity:[NSEntityDescription entityForName:@"Module" inManagedObjectContext:self.context]];
    NSArray * result = [self.context executeFetchRequest:fetch error:nil];
    allObjects = [allObjects arrayByAddingObjectsFromArray:result];
    
    [fetch setEntity:[NSEntityDescription entityForName:@"Post" inManagedObjectContext:self.context]];
    result = [self.context executeFetchRequest:fetch error:nil];
    allObjects = [allObjects arrayByAddingObjectsFromArray:result];
    
    [fetch setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:self.context]];
    result = [self.context executeFetchRequest:fetch error:nil];
    allObjects = [allObjects arrayByAddingObjectsFromArray:result];
    
    for (NSManagedObject *obj in allObjects)
    {
        [self.context deleteObject:obj];
    }
    
    [self.context save:nil];
}

- (void)loginWithCredentials:(NSString *)credentials modules:(NSString *)modules completionHandler:(void (^)(bool success))completionHandler
{
    if ([self loggedIn])
    {
        NSLog(@"already logged in");
        return;
    }
    if (!completionHandler)
    {
        return;
    }
    if (![self loginUserWithCredentials:credentials] || ![self addModules:modules])
    {
        completionHandler(NO);
        return;
    }
    
    completionHandler(YES);
}

- (BOOL)loginUserWithCredentials:(NSString *)credentials
{
    if (!credentials || credentials.length < 3)
    {
        return NO;
    }
    
    NSArray *credentialsArray = [credentials componentsSeparatedByString:StringSeparator];
    
    NSString *userID = credentialsArray[0];
    NSString *name = [[credentialsArray subarrayWithRange:NSMakeRange(1, credentialsArray.count-1)] componentsJoinedByString:@" "];
    
    [User createMainUserWithName:name userID:userID];
    
    return YES;
}

- (BOOL)addModules:(NSString *)modules
{
    if (!modules || !modules.length)
    {
        return NO;
    }
    
    NSArray *modulesArray = [modules componentsSeparatedByString:StringSeparator];
    if (modulesArray.count % 2)
    {
        return NO;
    }
    for (int q=0; q<modulesArray.count; q+=2)
    {
        [User addModuleToMainUserWithName:modulesArray[q+1] moduleCode:modulesArray[q]];
    }
    
    return YES;
}


@end
