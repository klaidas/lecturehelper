//
//  ConnectionManager.h
//  lecturehelper
//
//  Created by kls on 11/14/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Module.h"

@interface ConnectionManager : NSObject

- (void)fetchDataForModule:(NSString *)module lectureType:(NSString *)type completionHandler:(void(^)(NSData *, NSURLResponse *, NSError *))completionHandler;
- (void)createMessage:(NSString *)message module:(NSString *)module type:(NSString *)type completionHandler:(void(^)(NSData *, NSURLResponse *, NSError *))completionHandler;

@end
