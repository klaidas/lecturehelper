//
//  SingleModuleTableViewController.m
//  lecturehelper
//
//  Created by kls on 11/14/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "SingleModuleTableViewController.h"
#import "ConnectionManager.h"
#import "TextViewController.h"

#define MessageCell @"MessageCell"
#define SegmentedControlCell @"SegmentedControlCell"
#define SegmentedControlCellTag 1

@interface SingleModuleTableViewController () <TextViewControllerDelegate>

@property (strong, nonatomic) ConnectionManager *connectionManager;

@property (weak , nonatomic) UISegmentedControl *segmentedControl;
@property (strong, nonatomic) NSArray *items;

@end

@implementation SingleModuleTableViewController

- (ConnectionManager *)connectionManager
{
    if (!_connectionManager)
    {
        _connectionManager = [ConnectionManager new];
    }
    return _connectionManager;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchData];
}

- (void)textWasTyped:(NSString *)string
{
    __weak typeof(self) weakSelf = self;
    [self.connectionManager createMessage:string module:self.module type:[self typeString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [weakSelf fetchData];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueText"])
    {
        TextViewController *vc = segue.destinationViewController;
        vc.delegate = self;
    }
}

- (NSString *)typeString
{
    NSString *type = nil;
    
    if (self.segmentedControl)
    {
        switch (self.segmentedControl.selectedSegmentIndex)
        {
            case 1:
                type = @"theory";
                break;
            case 2:
                type = @"seminar";
                break;
            case 3:
                type = @"lab";
                break;
            case 4:
                type = @"general";
                break;
                
            default:
                break;
        }
    }
    return type;
}

- (void)fetchData
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //Add some method process in global queue - normal for data processing
        

        
        //Add some method process in global queue - normal for data processing
        
    
    
    
    __weak typeof(self) weakSelf = self;
    [self.connectionManager fetchDataForModule:self.module lectureType:[self typeString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        
        weakSelf.items = dataArray;
        
        NSLog(@"%@", dataArray);
        dispatch_async(dispatch_get_main_queue(), ^(){
            //Add method, task you want perform on mainQueue
            //Control UIView, IBOutlet all here
            [weakSelf.tableView reloadData];
            
        });
        
        
    }];
        });
    
}
#pragma mark - Table view data source

- (void)configureSegmentedControl:(UISegmentedControl *)segmentedControl
{
    if (!segmentedControl)
    {
        NSLog(@"SegmentedControl cell not found");
        return;
    }
    self.segmentedControl = segmentedControl;
    [self.segmentedControl addTarget:self action:@selector(segmentedControlSelectionDidChange:) forControlEvents:UIControlEventValueChanged];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        return self.module;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else if (section == 1)
    {
        return self.items.count;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.section == 0)
    {
        
        if (indexPath.row == 0)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:SegmentedControlCell forIndexPath:indexPath];
            [self configureSegmentedControl:[cell viewWithTag:SegmentedControlCellTag]];
        }

    }
    else if (indexPath.section == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:MessageCell forIndexPath:indexPath];
        cell.textLabel.text = self.items[indexPath.row][@"message_content"];
        cell.detailTextLabel.text = self.items[indexPath.row][@"user_name"];
    }
    
    return cell;
}

#pragma mark - SegmentedControl delegate

- (void)segmentedControlSelectionDidChange:(UISegmentedControl *)segmentedControl
{
    [self fetchData];
}



@end
