//
//  TextViewController.m
//  LectureHelper
//
//  Created by kls on 11/25/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "TextViewController.h"

@interface TextViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation TextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)sendA:(id)sender
{
    if (!self.textField.text.length)
    {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(textWasTyped:)])
    {
        [self.delegate textWasTyped:self.textField.text];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)sendAction:(UIBarButtonItem *)sender
{

}

@end
