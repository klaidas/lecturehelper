//
//  SingleModuleTableViewController.h
//  lecturehelper
//
//  Created by kls on 11/14/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Module.h"

@interface SingleModuleTableViewController : UITableViewController

typedef NS_ENUM(NSInteger, SingleModuleTableViewControllerSegment)
{
    SingleModuleTableViewControllerSegmentAll,
    SingleModuleTableViewControllerSegmentTheory,
    SingleModuleTableViewControllerSegmentPractice,
    SingleModuleTableViewControllerSegmentLaboratory
};

@property (weak, nonatomic) NSString *module;

@end
