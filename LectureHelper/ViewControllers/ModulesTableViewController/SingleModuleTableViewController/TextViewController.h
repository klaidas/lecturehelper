//
//  TextViewController.h
//  LectureHelper
//
//  Created by kls on 11/25/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TextViewController;

@protocol TextViewControllerDelegate <NSObject>

- (void)textWasTyped:(NSString *)string;

@end

@interface TextViewController : UIViewController

@property (weak, nonatomic) id<TextViewControllerDelegate> delegate;

@end
