//
//  ModulesTableViewController.m
//  lecturehelper
//
//  Created by kls on 11/13/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "ModulesTableViewController.h"
#import "User.h"
#import "CoreManager.h"
#import "SingleModuleTableViewController.h"

#define ShowModuleSegue @"ShowModuleSegue"

@interface ModulesTableViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@end

@implementation ModulesTableViewController


#pragma mark - TableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [User mainUser].name;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [User mainUser].modules.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Module *module = [[User mainUser].modules allObjects][indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = module.name;
    cell.detailTextLabel.text = module.moduleID;
    
    return cell;

}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:ShowModuleSegue])
    {
        if ([segue.destinationViewController isKindOfClass:[SingleModuleTableViewController class]])
        {
            SingleModuleTableViewController *destinationSegue = segue.destinationViewController;
            Module *mod = [[[User mainUser].modules allObjects] objectAtIndex:[self.tableView indexPathForCell:sender].row];
            destinationSegue.module = mod.moduleID;
        }
    }
}

@end
