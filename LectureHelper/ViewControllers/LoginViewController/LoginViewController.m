//
//  ViewController.m
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/2/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "LoginViewController.h"
#import "CoreManager.h"

#define LoginURL @"https://uais.cr.ktu.lt/ktuis/stp_prisijungimas"
#define DestinationURL @"https://uais.cr.ktu.lt/ktuis/stud.busenos"

#define JSScriptParseCredentials @"document.getElementsByClassName('navbar')[1].getElementsByClassName('navbar-text pull-right')[0].innerHTML.split(' ').slice(0, 3).toString();"
#define JSScriptParseModules @"(function(){var modules = document.getElementsByTagName('table')[0].getElementsByTagName('td'); var modulesArray = new Array(); var string = new String(); for (i = 0; i < modules.length; i++) { string = modules[i].innerHTML; string = string.replace('<b>', ''); string = string.replace('</b>', '');if (string){ modulesArray.push(string);}} return modulesArray.toString();})();"
//function myFunction() {
//
//    var modules = document.getElementsByTagName("table")[0].getElementsByTagName("td");
//    var modulesArray = new Array();
//    var string = new String();
//
//    for (i = 0; i < modules.length; i++)
//    {
//        string = modules[i].innerHTML;
//        string = string.replace('<b>', '');
//        string = string.replace('</b>', '');
//        if (string)
//        {
//            modulesArray.push(string);
//        }
//    }
//
//    return modulesArray.toString();
//}
#define LoginSegue @"LoginSegue"

@interface LoginViewController ()<UIWebViewDelegate>


@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) CoreManager *coreManager;

@end

@implementation LoginViewController

#pragma mark - Init/Setup

- (CoreManager *)coreManager
{
    if (!_coreManager)
    {
        _coreManager = [CoreManager sharedInstance];
    }
    return _coreManager;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configure];
}

- (void)configure
{
    [self.navigationController setNavigationBarHidden:YES];
    [self.coreManager logout];
    [self loadLoginPage];
}

- (void)loadLoginPage
{
    if (!self.webView.delegate)
    {
        self.webView.delegate = self;
    }
    
    NSMutableURLRequest  *request = [NSMutableURLRequest  requestWithURL:[NSURL URLWithString:LoginURL]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [self.webView loadRequest:request];
}

#pragma mark - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([webView.request.URL.absoluteString isEqualToString:DestinationURL])
    {
        NSString *credentials = [self.webView stringByEvaluatingJavaScriptFromString:JSScriptParseCredentials];
        NSString *modules = [self.webView stringByEvaluatingJavaScriptFromString:JSScriptParseModules];
        
        [self.coreManager loginWithCredentials:credentials modules:modules completionHandler:^(BOOL success) {
            if (success)
            {
                NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
                for (NSHTTPCookie *cookie in [storage cookies]) {
                    [storage deleteCookie:cookie];
                }
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self performSegueWithIdentifier:LoginSegue sender:self];
            }
            else
            {
//                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@":(" message:@"Something went bad" preferredStyle:UIAlertControllerStyleAlert];
//                [[UIApplication sharedApplication].keyWindow.rootViewController.presentedViewController presentViewController:alertController animated:YES completion:^{
//                    [weakSelf loadLoginPage];
//                }];
            }
        }];
    }
}


@end
