//
//  AppDelegate.h
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/2/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

