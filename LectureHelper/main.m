//
//  main.m
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/2/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
