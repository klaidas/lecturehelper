//
//  Post+CoreDataProperties.h
//  lecturehelper
//
//  Created by kls on 11/25/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Post.h"

NS_ASSUME_NONNULL_BEGIN

@interface Post (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSString *attachment;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *module_code;
@property (nullable, nonatomic, retain) NSString *user_name;
@property (nullable, nonatomic, retain) Module *module;

@end

NS_ASSUME_NONNULL_END
