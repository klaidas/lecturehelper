//
//  Module+CoreDataProperties.m
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/10/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Module+CoreDataProperties.h"

@implementation Module (CoreDataProperties)

@dynamic name;
@dynamic moduleID;
@dynamic user;
@dynamic posts;

@end
