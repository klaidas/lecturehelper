//
//  Module.h
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/10/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define ModuleClassName @"Module"

@class Post, User;

NS_ASSUME_NONNULL_BEGIN

@interface Module : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Module+CoreDataProperties.h"
