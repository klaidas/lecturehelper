//
//  User.h
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/10/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define UserClassName @"User"

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

+ (User *)mainUser;
+ (void)createMainUserWithName:(NSString *)name userID:(NSString *)userID;
+ (void)addModuleToMainUserWithName:(NSString *)name moduleCode:(NSString *)moduleID;

- (NSString *)displayName;

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
