//
//  Post.h
//  lecturehelper
//
//  Created by kls on 11/25/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Module;

NS_ASSUME_NONNULL_BEGIN

@interface Post : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Post+CoreDataProperties.h"
