//
//  User.m
//  LectureHelper
//
//  Created by Klaidas Strazdauskas on 10/10/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//

#import "User.h"
#import "CoreManager.h"

@implementation User

+ (User *)mainUser
{
    NSError *error = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:UserClassName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"mainUser == YES"];
    
    NSArray *fetchedUsers = [[CoreManager sharedInstance].context executeFetchRequest:fetchRequest error:&error];
    if (error)
    {
        NSLog(@"%@", error);
    }
    int usersCount = (int)fetchedUsers.count;
    
    if (usersCount == 0)
    {
        return nil;
    }
    else if (usersCount > 1)
    {
        NSLog(@"error, more >1 main useres");
        return nil;
    }
    
    return fetchedUsers.firstObject;
}

+ (void)createMainUserWithName:(NSString *)name userID:(NSString *)userID
{
    User *user = [NSEntityDescription insertNewObjectForEntityForName:UserClassName inManagedObjectContext:[CoreManager sharedInstance].context];
    user.name = name;
    user.userID = userID;
    user.mainUser = @YES;
    
    [[CoreManager sharedInstance].context save:nil];
}

+ (void)addModuleToMainUserWithName:(NSString *)name moduleCode:(NSString *)moduleID
{
    User *user = [self mainUser];
    
    Module *module = [NSEntityDescription insertNewObjectForEntityForName:ModuleClassName inManagedObjectContext:[CoreManager sharedInstance].context];
    module.name = name;
    module.moduleID = moduleID;
    
    module.user = user;
    
    [[CoreManager sharedInstance].context save:nil];
}

- (NSString *)displayName
{
    return self.name;
}

@end
