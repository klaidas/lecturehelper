//
//  Post+CoreDataProperties.m
//  lecturehelper
//
//  Created by kls on 11/25/15.
//  Copyright © 2015 Klaidas Strazdauskas. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Post+CoreDataProperties.h"

@implementation Post (CoreDataProperties)

@dynamic type;
@dynamic attachment;
@dynamic content;
@dynamic date;
@dynamic id;
@dynamic module_code;
@dynamic user_name;
@dynamic module;

@end
